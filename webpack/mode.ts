import {Env} from './env.helper';

export function getMode(env: Env): 'development' | 'production' | 'none' {
    if (env === Env.Production) {
        return 'production';
    }
    return 'development';
}
