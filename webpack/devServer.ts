import {Env} from './env.helper';
import * as path from 'path';
import {appPath} from './constants';

export function getDevServer(env: Env) {
    if (env === Env.Development) {
        return {
            host: '0.0.0.0',
            disableHostCheck: true,
            historyApiFallback: true,
            contentBase: path.join(appPath, 'resources'),
        };
    } else {
        return undefined;
    }
}
