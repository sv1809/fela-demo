import {getWebpackPlugins} from './plugins';
import {getWebpackModules} from './modules';
import {getEnv} from './env.helper';
import {getDevTool} from './devTool';
import {getDevServer} from './devServer';
import {appPath, distPath} from './constants';
import {getMode} from './mode';

const env = getEnv();

const config = {
    context: appPath,
    entry: {
        js: [
            `${appPath}/index.tsx`,
        ],
    },
    output: {
        filename: '[name].[chunkhash].js',
        chunkFilename: '[name].[chunkhash].js',
        path: distPath,
        publicPath: '/'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
        modules: ['node_modules'],
    },
    devtool: getDevTool(env),
    module: getWebpackModules(),
    plugins: getWebpackPlugins(),
    devServer: getDevServer(env),
    mode: getMode(env),
};

// tslint:disable-next-line:no-default-export
export default config;
