import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import {appPath} from './constants';
import * as ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import * as path from 'path';

const htmlWebpackPlugin = new HtmlWebpackPlugin({
    template: `${appPath}/index.html`,
    filename: 'index.html',
    inject: 'body',
});

const forkTsCheckerWebpackPlugin = new ForkTsCheckerWebpackPlugin({
    tsconfig: path.join(__dirname, '../tsconfig.json'),
});

export function getWebpackPlugins() {
    return [
        htmlWebpackPlugin,
        forkTsCheckerWebpackPlugin,
    ];
}
