export enum Env {
    Production = 'production',
    Development = 'development'
}

export function getEnv(): Env {
    const env = process.env.NODE_ENV;
    switch (env) {
        case 'production':
            return Env.Production;
        default:
            return Env.Development;
    }
}

export function geStringEnvFromNODE_ENV(): string {
    return JSON.stringify(process.env.NODE_ENV);
}
