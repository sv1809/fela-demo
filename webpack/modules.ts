import {Module} from 'webpack';

export function getWebpackModules(): Module {
    return {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    transpileOnly: true,
                },
            },
        ]
    };
}
