import {Env} from './env.helper';
import {Options} from 'webpack';

export function getDevTool(env: Env): Options.Devtool {
    switch (env) {
        case Env.Development:
            return 'eval-source-map';
        default:
            return undefined;
    }
}
