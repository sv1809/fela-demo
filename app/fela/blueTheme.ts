import {Theme} from './theme.interface';

export const blueTheme: Theme = {
	mainColor: '#7eb6ec',
	textDisabledOpacity: .5,
};
