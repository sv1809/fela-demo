import {Theme} from './theme.interface';

export interface FelaWithThemeProps {
	theme: Theme;
}