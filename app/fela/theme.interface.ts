export interface Theme {
	mainColor: string;
	textDisabledOpacity: number;
}