import {Theme} from './theme.interface';

export const greenTheme: Theme = {
	mainColor: '#16c565',
	textDisabledOpacity: .5,
};
