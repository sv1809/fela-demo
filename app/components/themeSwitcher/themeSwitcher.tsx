import * as React from 'react';
import {connect} from 'react-fela';
import {ThemeSwitcherOwnProps, ThemeSwitcherProps, ThemeSwitcherStyles} from './themeSwitcher.interface';
import {styleThemeSwitcher} from './themeSwitcher.style';
import {ThemeNames} from '../../fela/theme.helpers';
import {ThemeSwitcherElement} from './components/themeSwitcherElement/themeSwitcherElement';

class ThemeSwitcherComponent extends React.Component<ThemeSwitcherProps>{

	public render() {
		const {currentThemeName, styles} = this.props;

		return (
			<div className={styles.root}>
				<ThemeSwitcherElement
					onClick={this.onThemeSwitcherElementClick}
					active={currentThemeName === ThemeNames.Blue}
					text="Blue"
					themeName={ThemeNames.Blue}
				/>
				<ThemeSwitcherElement
					onClick={this.onThemeSwitcherElementClick}
					active={currentThemeName === ThemeNames.Green}
					text="Green"
					themeName={ThemeNames.Green}
				/>
			</div>
		);
	}

	private onThemeSwitcherElementClick = (themeName: ThemeNames) => {
		const {currentThemeName, onThemeChange} = this.props;
		if(currentThemeName !== themeName) {
			onThemeChange(themeName);
		}
	}

}

export const ThemeSwitcher = connect<ThemeSwitcherOwnProps, ThemeSwitcherStyles>(styleThemeSwitcher)(ThemeSwitcherComponent);