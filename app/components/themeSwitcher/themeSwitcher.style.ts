import {Rules} from "react-fela";
import {Theme} from '../../fela/theme.interface';
import {ThemeSwitcherOwnProps, ThemeSwitcherStyles} from './themeSwitcher.interface';

export const styleThemeSwitcher: Rules<ThemeSwitcherOwnProps, ThemeSwitcherStyles, Theme> = () => {
	return {
		root: {
			display: 'flex',
		},
	};
};
