import * as React from 'react';
import {connect} from 'react-fela';
import {
	ThemeSwitcherElementOwnProps,
	ThemeSwitcherElementProps,
	ThemeSwitcherElementStyles,
} from './themeSwitcherElement.interface';
import {styleThemeSwitcherElement} from './themeSwitcherElement.style';

class ThemeSwitcherElementComponent extends React.Component<ThemeSwitcherElementProps> {

	public render() {
		const {styles, text} = this.props;

		return (
			<div onClick={this.onClick} className={styles.root}>
				{text}
			</div>
		);
	}

	private onClick = () => {
		const {themeName, onClick} = this.props;
		onClick(themeName);
	}

}

export const ThemeSwitcherElement = connect<ThemeSwitcherElementOwnProps, ThemeSwitcherElementStyles>(styleThemeSwitcherElement)(ThemeSwitcherElementComponent);