import {Rules} from "react-fela";
import {ThemeSwitcherElementOwnProps, ThemeSwitcherElementStyles} from './themeSwitcherElement.interface';
import {Theme} from '../../../../fela/theme.interface';

export const styleThemeSwitcherElement: Rules<ThemeSwitcherElementOwnProps, ThemeSwitcherElementStyles, Theme> = (props) => {
	const {theme, active} = props;
	return {
		root: {
			color: active
				? theme.mainColor
				: 'default',
			cursor: active
				? 'default'
				: 'pointer',
			marginRight: '10px',
			':hover': {
				textDecoration: active
					? 'default'
					: 'underline',
			},
		},
	};
};
