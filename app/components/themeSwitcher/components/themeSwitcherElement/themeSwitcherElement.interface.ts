import {CSSProperties} from 'react';
import {FelaWithStylesProps} from 'react-fela';
import {ThemeNames} from '../../../../fela/theme.helpers';

export interface ThemeSwitcherElementStyles {
	root: CSSProperties;
}

export interface ThemeSwitcherElementOwnProps {
	active?: boolean;
	onClick: (themeName: ThemeNames) => void;
	text: string;
	themeName: ThemeNames;
}

export type ThemeSwitcherElementProps =
	ThemeSwitcherElementOwnProps
	& FelaWithStylesProps<ThemeSwitcherElementOwnProps, ThemeSwitcherElementStyles>;