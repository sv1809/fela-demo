import {CSSProperties} from 'react';
import {FelaWithStylesProps} from 'react-fela';
import {ThemeNames} from '../../fela/theme.helpers';

export interface ThemeSwitcherStyles {
	root: CSSProperties;
}

export interface ThemeSwitcherOwnProps {
	currentThemeName: ThemeNames;
	onThemeChange: (themeName: ThemeNames) => void;
}

export type ThemeSwitcherProps = ThemeSwitcherOwnProps & FelaWithStylesProps<ThemeSwitcherOwnProps, ThemeSwitcherStyles>;