import * as React from 'react';
import {connect} from 'react-fela';
import {LabelsOwnProps, LabelsProps, LabelsStyles} from './labels.interface';
import {styleLabels} from './labels.style';
import {Label} from '../label/label';

class LabelsComponent extends React.Component<LabelsProps> {

	public render() {
		const {styles} = this.props;

		return (
			<div className={styles.root}>
				<Label>Enabled</Label>
				<Label disabled>Disabled</Label>
			</div>
		);
	}

}

export const Labels = connect<LabelsOwnProps, LabelsStyles>(styleLabels)(LabelsComponent);