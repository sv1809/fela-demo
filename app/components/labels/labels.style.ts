import {Rules} from "react-fela";
import {LabelsOwnProps, LabelsStyles} from './labels.interface';
import {Theme} from '../../fela/theme.interface';

export const styleLabels: Rules<LabelsOwnProps, LabelsStyles, Theme> = () => {
	return {
		root: {
			display: 'flex',
			marginTop: '10px',
		},
	};
};
