import {CSSProperties} from 'react';
import {FelaWithStylesProps} from 'react-fela';

export interface LabelsStyles {
	root: CSSProperties;
}

export interface LabelsOwnProps {
}

export type LabelsProps =
	LabelsOwnProps
	& FelaWithStylesProps<LabelsOwnProps, LabelsStyles>;