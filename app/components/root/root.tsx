// tslint:disable-next-line:no-unused-variable
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {RendererProvider as FelaProvider, ThemeProvider} from 'react-fela';
import {RootState} from './root.interface';
import {renderer} from '../../fela/renderer';
import {ThemeNames} from '../../fela/theme.helpers';
import {blueTheme} from '../../fela/blueTheme';
import {greenTheme} from '../../fela/greenTheme';
import {ThemeSwitcher} from '../themeSwitcher/themeSwitcher';
import {Labels} from '../labels/labels';

export class Root extends React.Component<{}, RootState> {

	public state: RootState = {
		themeName: ThemeNames.Blue,
	};

	public render() {
		const {themeName} = this.state;

		const theme = themeName === ThemeNames.Blue
			? blueTheme
			: greenTheme;

		return (
			<FelaProvider renderer={renderer}>
				<ThemeProvider theme={theme}>
					<div>
						<ThemeSwitcher currentThemeName={themeName} onThemeChange={this.changeTheme}/>
						<Labels/>
					</div>
				</ThemeProvider>
			</FelaProvider>
		)
	}

	private changeTheme = (themeName: ThemeNames) => {
		this.setState({
			themeName,
		})
	}

}

ReactDOM.render(<Root/>, document.getElementById('root'));
