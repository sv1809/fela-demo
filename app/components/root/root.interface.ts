import {ThemeNames} from '../../fela/theme.helpers';

export interface RootState {
	themeName: ThemeNames;
}