import {Rules} from "react-fela";
import {LabelOwnProps, LabelStyles} from './label.interface';
import {Theme} from '../../fela/theme.interface';

export const styleLabel: Rules<LabelOwnProps, LabelStyles, Theme> = (props) => {
	const {theme, disabled} = props;
	return {
		root: {
			opacity: disabled
				? theme.textDisabledOpacity
				: 1,
			marginRight: '10px',
		},
	};
};
