import * as React from 'react';
import {connect} from 'react-fela';
import {
	LabelOwnProps,
	LabelProps,
	LabelStyles,
} from './label.interface';
import {styleLabel} from './label.style';

class LabelComponent extends React.Component<LabelProps> {

	public render() {
		const {styles, children} = this.props;

		return (
			<div className={styles.root}>
				{children}
			</div>
		);
	}

}

export const Label = connect<LabelOwnProps, LabelStyles>(styleLabel)(LabelComponent);