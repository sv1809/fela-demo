import {CSSProperties} from 'react';
import {FelaWithStylesProps} from 'react-fela';

export interface LabelStyles {
	root: CSSProperties;
}

export interface LabelOwnProps {
	disabled?: boolean;
}

export type LabelProps =
	LabelOwnProps
	& FelaWithStylesProps<LabelOwnProps, LabelStyles>;